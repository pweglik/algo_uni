#LIS in O(n logn)


def lis(A):
    n = len(A)
    S = [0]
    index_biggest = 0
    P = [-1] * n

    for i in range(1,n):
        if A[i] > A[S[index_biggest]]:
            S.append(i)
            P[i] = S[index_biggest]
            index_biggest += 1
            
        # 1, 2, 4, 5, 8  --- 3
        elif A[i] < A[S[0]]:
            S = []
            S.append(i)
            index_biggest = 0

        else:
            l = S[0]
            r = index_biggest
            while l <= r:
                mid = (l + r) // 2

                if ( mid == 0 and A[S[0]] >= A[i] ) or ( A[S[mid]] >= A[i] and A[S[mid - 1]] < A[i]):
                    S[mid] = i
                    P[i] = S[mid - 1]
                    break
                
                elif S[mid] > A[i]:
                    r = mid - 1
                
                else:
                    l = mid + 1

    print(A)
    print(S)
    print(P)
    print_solution(A, P, S[index_biggest])
    print()

def print_solution(A, P, index):
    if P[index] != -1:
        print_solution(A, P, P[index])
    
    print(A[index], end = ' ')


    

A = [2, 6, 3, 4, 0, 1, 2, 3, 9, 5, 8]
lis(A)

