#Knapsack problem using dynamic programming

# O(n * maxW)
def knapsack(P, W, MaxW):
    n = len(P)
    F = [[0] * (MaxW + 1) for i in range(n)]
    for w in range(W[0], MaxW + 1):
        F[0][w] = P[0]

    for i in range(1, n):
        for w in range(1, MaxW + 1):
            F[i][w] = F[i - 1][w]
            if w >= W[i]:
                F[i][w] = max(F[i][w], F[i-1][w - W[i]] + P[i])
            
    return F[n-1][MaxW], F


P = [ 10, 8, 4, 5, 3, 7]
W = [4, 5, 12, 9, 1, 13]
A = [3,4,5,2]
W_A = [3,6,2,3]
maxA = 5
MaxW = 24

maxP, F = knapsack(P, W, MaxW)
print(maxP)
for row in F:
    print(row)


# F(i)- minimalna waga przedmiotów o wartości i
# O (n * sum of P)
values = list(map(int,input().split()))
weights = list(map(int,input().split()))
w = int(input())
T=[-1]*(sum(values)+1)
T[0]=0
for i in range(len(values)):
    for j in range(len(T)-1,-1,-1):
        if T[j]>=0:
            if T[j+values[i]]==-1:
                T[j+values[i]]=T[j]+weights[i]
            else:
                T[j+values[i]]=min(T[j+values[i]],T[j]+weights[i])
maks = 0
for i in range(len(T)):
    if T[i]<=w and T[i]>=0:
        maks=max(maks,i)

print(maks)



