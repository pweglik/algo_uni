# O(n * T), where T is target sum
def subset_sum(num_set, sum):
	n = len(num_set)
	subset =[[False] * (sum + 1) for i in range(n + 1)]
	
	for i in range(n + 1):
		subset[i][0] = True
		
	for i in range(1, sum + 1):
		subset[0][i]= False
			
	for i in range(1, n + 1):
		for j in range(1, sum + 1):
			if j < num_set[i - 1]:
				subset[i][j] = subset[i - 1][j]
			if j >= num_set[i - 1]:
				subset[i][j] = (subset[i - 1][j] or
								subset[i - 1][j - num_set[i - 1]])
	
	return subset[n][sum]

num_set = [3,4,5,2]	
print(subset_sum(num_set, 6))
