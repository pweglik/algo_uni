from math import *


def partition(arr, l, r): 
	i = (l-1)		 # index of smaller element 
	pivot = arr[r][1]	 # pivot 

	for j in range(l, r): 

		# If current element is smaller than or 
		# equal to pivot 
		if arr[j][1] <= pivot: 

			# increment index of smaller element 
			i = i+1
			arr[i], arr[j] = arr[j], arr[i] 

	arr[i+1], arr[r] = arr[r], arr[i+1] 
	return i+1


def quick_sort(arr, l, r): 
    while l < r:
        pi = partition(arr, l, r)

        if pi - l < r - pi:
        
            quick_sort(arr, l, pi - 1)
            l = pi + 1
        
        else:
            quick_sort(arr, pi + 1, r)
            r = pi - 1


def city_dist(c1, c2):
    return sqrt( (c1[1] - c2[1])** 2 + (c1[2] - c2[2])**2)



def bitonicTSP( C ):
    n = len(C)
    quick_sort(C, 0, n - 1)
    D = [ [ city_dist(C[i], C[j]) for j in range(n)] for i in range(n)]
    F = [[inf] * n for _  in range(n)]
    F[0][1] = D[0][1]
    N = [[-1] * n for _  in range(n)]

    def tspf_recu(i, j, F, D):
        if F[i][j] != inf:
            return F[i][j]

        if i == j - 1:
            best = inf
            best_k = -1
            for k in range(j-1):
                minimum = min(best, tspf_recu(k, j - 1, F, D) + D[k][j] )
                if best > minimum:
                    best = minimum
                    best_k = k

            F[j-1][j] = best
            N[j-1][j] = best_k

        else:
            F[i][j] = tspf_recu(i, j - 1, F, D) + D[j - 1][j]
            N[i][j] = j-1

        return F[i][j]
    

    real_best = inf
    best_N = None

    for i in range(n -1):
        minimum = min(real_best, tspf_recu(i, n - 1, F, D) + D[i][n - 1] )
        if real_best > minimum:
            real_best = minimum
            best_N = N.copy()
    

    solution(C, best_N)
    print(real_best)


def solution(C, N):
    for row in N:
        print(row)
    n = len(C)
    sol = [[],[]]
    output = 0 # output side 0 or 1 - flipping when we get lower number
    sol[0].append(n-1)
    sol[1].append(n-2)

    i = n - 2
    j = n - 1
    while True:
        if N[i][j] == -1:
            break
        sol[output].append(N[i][j])

        if N[i][j] < i:
            temp = N[i][j]
            j = i
            i = temp
            output = not output 
        else:
            j = N[i][j]

    sol1, sol2 = sol
    if sol1[-1] != 0:
        sol1.append(0)
    sol1.reverse()

    if sol2[-1] != 0:
        sol2.append(0)

    sol1.extend(sol2)

    for i in range(len(sol1)):
        if i != len(sol1) - 1:
            print(C[sol1[i]][0], end = ", ")
        else:
            print(C[sol1[i]][0], end = "\n")



C = [["1", 0, 1], ["2", 1, -6], ["3", 3, -1], ["4", 6, -2], ["5", 10, 1],      ["6", 14, 3], ["7", 9, 4], ["8", 7, 2], ["9", 4, 3], ["10", 2, 3]]
bitonicTSP(C)
# C = [["Wrocław", 0, 2], ["Warszawa", 4, 3], ["Gdańsk", 2, 4], ['Paprykarz', 5, 2], ["Kraków", 3, 1]]
# bitonicTSP(C)
# C = [['A', 0, 2], ['B', 1, 1], ['C', 4, 1], ['D', 5, 3], ['E', 6, 3], ['F', 8, 3], ['G', 7, 4], ['H', 2, 4], ['I', 0.5, 2.5], ['J', 1.5, 3.5]]
# bitonicTSP(C)
# C8 = [['s', 9, 24], ['e', 11, 2], ['y', -5, 26], ['a', 28, -17], ['i', 23, 11], ['W', -24, -24], ['h', 29, 24],
#      ['*', -25, 27], ['U', 22, -16], ['b', 5, 2], ['j', 15, -25], ['s', 24, -10], ['i', -9, 9], ['k', 18, 4],
#      ['e', -19, 10], ['t', 16, -3], ['W', 30, 10], ['c', 26, 11], ['s', -20, -17], ['z', -17, 27]]
# bitonicTSP(C8)
# C = [
#     ["Wrocław", 0, 1],
#     ["Warszawa", 11, 5],
#     ["Gdańsk", 4, 2],
#     ["Kraków", 2, 1],
#     ["Papr", 7, 3],
#     ["kox", 0.5, 4]]
# print(bitonicTSP(C))
# C = [
#     ["Wrocław", 0, 12],
#     ["Warszawa", 1, 3],
#     ["Gdańsk", 2, 8],
#     ["Kraków", 12, -5],
#     ["Papr", 4, -1],
#     ["kox", 8, 9]]
# print(bitonicTSP(C))
# C = [
#     ["0-", 1, 1],
#     ["1-", 2, 4],
#     ["2-", 3, 6],
#     ["3-", 4, 2],
#     ["4-", 5, 6],
#     ["5-", 7, 6],
#     ["6-", 8, 4]]
# print(bitonicTSP(C))
# C = [['A', 0, 2], [
#     'B', 1, 1], [
#     'C', 4, 1], [
#     'D', 5, 3], [
#     'E', 6, 3], [
#     'F', 8, 3], [
#     'G', 7, 4], [
#     'H', 2, 4], [
#     'I', 0.5, 2.5], [
#     'J', 1.5, 3.5]]
# print(bitonicTSP(C))
# C0 = [["Wrocław", 0, 2], ["Warszawa", 4, 3], ["Gdańsk", 2, 4], ["Kraków", 3, 1], ["Paprykarz", 6, 2], ["Palcza", 7, 3]]
# C1 = [["Wrocław", 0, 2], ["Warszawa", 4, 3], ["Gdańsk", 2, 4], ["Kraków", 3, 1], ["Papr", 1, 3], ["kox", 0.5, -2]]
# C2 = [["Wrocław", 0, 1], ["Warszawa", 11, 5], ["Gdańsk", 4, 2], ["Kraków", 2, 1], ["Papr", 7, 3], ["kox", 0.5, 4]]
# C3 = [["Wrocław", 0, 12], ["Warszawa", 1, 3], ["Gdańsk", 2, 8], ["Kraków", 12, -5], ["Papr", 4, -1], ["kox", 8, 9]]
# C4 = [['A', 0, 2], ['B', 1, 1], ['C', 4, 1], ['D', 5, 3], ['E', 6, 3], ['F', 8, 3], ['G', 7, 4], ['H', 2, 4],
#       ['I', 0.5, 2.5], ['J', 1.5, 3.5]]
# C5 = [["A", 0, 2], ["B", 4, 3], ["C", 2, 4], ["D", 3, 1], ["E", 1, 3], ["F", 0.5, -2]]
# C6 = [["Wrocław", 0, 2], ["Warszawa",4,3], ["Gdańsk", 2,4], ["Kraków",3,1]]
# C7 = [["1", 0, 1], ["2", 1, -6], ["3", 3, -1], ["4", 6, -2], ["5", 10, 1], ["6", 14, 3], ["7", 9, 4], ["8", 7, 2], ["9", 4, 3], ["10", 2, 3]]
# print(bitonicTSP(C1))
# print(bitonicTSP(C2))
# print(bitonicTSP(C3))
# print(bitonicTSP(C4))
# print(bitonicTSP(C5))
# print(bitonicTSP(C6))
# print(bitonicTSP(C7))