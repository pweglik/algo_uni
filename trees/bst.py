class Tree:
    def __init__(self):
        self.root = None
    def insert(self, val):
        if self.root == None:
            self.root = BSTNode(val)
        else:
            cur = self.root
            while True:
                if val > cur.val:
                    if cur.right != None:
                        cur = cur.right
                    else:
                        cur.right = BSTNode(val, cur)
                        break
                elif val < cur.val:
                    if cur.left != None:
                        cur = cur.left
                    else:
                        cur.left = BSTNode(val, cur)
                        break
                else:
                    break

    def find(self, val):
        cur = self.root
        while True:
            if val > cur.val:
                if cur.right != None:
                    cur = cur.right
                else:
                    cur = None
                    break
            elif val < cur.val:
                if cur.left != None:
                    cur = cur.left
                else:
                    cur = None
                    break
            else:
                break
        return cur

    def min(self, node = None):
        if node == None:
            cur = self.root
        else:
            cur = node

        while cur.left != None:
            cur = cur.left
        return cur

    def max(self, node = None):
        if node == None:
            cur = self.root
        else:
            cur = node

        while cur.right != None:
            cur = cur.right
        return cur

    def prev(self, val):
        cur = self.find(val)
        if cur.left != None:
            cur = cur.left
            cur = self.max(cur)
        else:
            if cur.parent == None:
                return None
            while True:
                prev = cur
                cur = cur.parent
                if cur.right == prev:
                    break
        return cur

    def next(self, val):
        cur = self.find(val)
        if cur.left != None:
            cur = cur.right
            cur = self.min(cur)
        else:
            if cur.parent == None:
                return None
            while True:
                prev = cur
                cur = cur.parent
                if cur.left == prev:
                    break
        return cur

    def remove(self, val):
        cur = self.find(val)
        if cur == None:
            return None


        if cur.left == None and cur.right == None:
            if cur == self.root:
                self.root = None
            else:
                par = cur.parent
                if par.left == cur:
                    par.left = None
                elif par.right == cur:
                    par.right = None

                cur = None

        elif cur.left == None:
            if cur == self.root:
                self.root = cur.right
            else:
                par = cur.parent
                if par.left == cur:
                    par.left = cur.right
                elif par.right == cur:
                    par.right = cur.right

                cur.right.parent = par
                cur = None

        elif cur.right == None:
            if cur == self.root:
                self.root = cur.left
            else:
                par = cur.parent
                if par.left == cur:
                    par.left = cur.left
                elif par.right == cur:
                    par.right = cur.left

                cur.left.parent = par
                cur = None
        
        else:
            suc = self.next(cur.val)
            temp = suc.val
            self.remove(suc.val)
            cur.val = temp



    def __str__(self):
        string = ""
        if self.root != None:
            string += str(self.root) 
        
        return string


class BSTNode:
    def __init__(self, val = 0, parent = None, left = None, right = None):
        self.val  = val
        self.parent = parent
        self.left = left
        self.right = right

    def str_rec(self, node, level = 0):
        strleft = ""
        strright = ""
        if node.left != None or node.right != None:
            if node.left != None:
                strleft = "\n" + self.str_rec(node.left, level + 1)
            else:
                strleft = "\n" + "-"*(level + 1)

            if node.right != None:
                strright = "\n" + self.str_rec(node.right, level + 1)
            else:
                strright = "\n" + "-"*(level + 1)
    
        return "-"*level + str(node.val) + strleft + strright

    def __str__(self):
        return self.str_rec(self, 0)