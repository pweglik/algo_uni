# Przemysław Węglik
# Algorytm działa w sposób anstępujący:
# idac przez liste od lewej strony za każdym razem patrzymy na k kolejnych nodów i znajdujemy najmniejsza wartosc
# poniewaz patrzylismy na k wartosci wiemy ze ten najmniejszy musi stac po lewej stronie tego przedzialu
# Przepinamy go tam, a potem przesuwamy nasz "przedzial patrzenia" o 1 w lewo, poniewaz wartosc po prawej jest juz posortowana
# powtarzamy tak idac przez cala liste, gdy zostalo nam do konca mniej niz k elementow mozemy posortowac pozostalosc np. quicksortem

# wykonamy n - k poszukiwan minimum a potem quicksorta na k wartosciach
# Zlozonosc czasowa: O( (n-k) * k + k*log(k))

# k = O(1)    O(n)
# k = O(logn) O(n* logn)
# k = O(n)    O(n* logn)

# niestety wkradł mi się błąd gdzieś w przepinaniu list, którego nie zdążyłem zdebugować


from zad2testy import runtests

class Node:
  def __init__(self):
    self.val = None     
    self.next = None 



def SortH(p,k):
    
    # quicksort on list for the last section, very important when k -> n
    def qsort( L ):
        # Subfunctions
        def rec_qsort(head, tail):
            if head != tail:
                ss, se, bs, be, ps, pe = partition(head, tail)
                
                
                if ss != None and se != None:
                    ss, se = rec_qsort(ss, se)
                    head = ss
                    se.next = ps
                    pe.next = None
                else:
                    head = ps
                    pe.next = None

                if bs != None and be != None:
                    bs, be = rec_qsort(bs, be)
                    pe.next = bs
                    tail = be
                    be.next = None
                else:
                    tail = pe
                    be.next = None

            return head, tail


        def partition(head, tail):
            cur = head
            pivot = head
            pivot_end = pivot

            # Add sentinel nodes, to remove amount of checking needed further
            # Delete them before merging lists
            small_start = Node(".")
            small_end = small_start
            big_start = Node(".")
            big_end = big_start
            
            # partitioning nodes into sublists smaller, bigger and equalu to pivot
            cur = cur.next
            while cur != tail.next:
                if cur.value < pivot.value:
                    small_end.next = cur
                    small_end = small_end.next

                elif cur.value > pivot.value:
                    big_end.next = cur
                    big_end = big_end.next

                else:
                    pivot_end.next = cur
                    pivot_end = pivot_end.next

                cur = cur.next
            
            #Deleting sentinel nodes
            small_start = small_start.next
            big_start = big_start.next

            return small_start, small_end, big_start, big_end, pivot, pivot_end

        cur = L
        while cur.next != None:
            cur = cur.next

        L, _ = rec_qsort(L, cur)

        return L

    to_return = None
    to_return_head = None
    cur = p
    prev = None
    while cur != None:

        counter = 0
        k_cur = cur
        while k_cur != None and counter < k:
            counter += 1
            k_cur = k_cur.next
        if counter != k:
            prev.next = qsort(cur)
        

        # gdzieś w przepinaniu blad - albo gubia sie nody, albo powstaje nieskonczone zapetlenia
        

        smallest = cur.val
        smallest_p = cur
        smallest_p_prev = prev

        k_cur = cur.next
        k_prev = k_cur
        while k_cur != None and counter < k:
            if k_cur.val < smallest:
                smallest = k_cur.val
                smallest_p_prev = k_prev
                smallest_p = k_cur

            k_prev = k_cur
            k_cur = k_cur.next

        if prev == None:
            # przepiecie tego przed wyciaganym do tego po wyciaganym
            if smallest_p_prev != None:
                smallest_p_prev.next = smallest_p.next

            to_return = smallest_p
            smallest_p_prev.next = smallest_p.next
            to_return.next = cur
            cur = to_return

        else:
            if prev.next != smallest_p:
                # przepiecie tego przed wyciaganym do tego po wyciaganym
                smallest_p_prev.next = smallest_p.next

                smallest_p.next = prev.next
                prev.next = smallest_p
                cur = smallest_p
            

        
        prev = cur
        cur = cur.next

    return to_return



runtests( SortH ) 