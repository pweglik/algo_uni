#Przemysław Węglik
# Pomysł polega na podzieleniu liczb z tablicy T na mniejsze tablice wg zadanych przedziałów
# a następnie posortowanie każdego przedziału używając normalizowanego bucketsorta
# czestotliwosc wykorzystujemy do znalezienia ilosci bucketów w danym przedziale wzorem: len(T) * czestotliwosc_przedzialu
# potem mergujemy mniejsze tablice (u mnie nazwane big_buckets) z powrotem do tablicy T

#Zlozonosc:
# obliczeniowa: O(n*k) z powodu: O(n*k) na rozlozenie liczb do big_bucketów, O(n*k) - bucket sort na k bucketach O(n) - mergowanie
# gdyby był to rozklad jednostajny mozna by użyć zwykłęgo bucket sorta, ale tutaj rozkład jednostajny wystepuje tylko w przedzialach
# jesli k -> n algorytm sie ukwadratawia, w przypadku duzych k lepiej uzyc quicksorta

# pamieciowa: O(n) na stworzenie big_buckets do przechowywania liczb

from zad3testy import runtests

def SortTab(T,P):

    # for sorting buckets
    def insertion_sort(arr):
        for i in range(len(arr)):
            smallest_index = i
            for j in range(i + 1, len(arr)):
                if arr[j] < arr[smallest_index]:
                    smallest_index = j
            arr[i], arr[smallest_index] = arr[smallest_index], arr[i]

    def bucket_sort(arr, num):
        # max and min are O(n) operaions, can be done in for loop
        max_ele = max(arr)
        min_ele = min(arr)
    
        val_range = (max_ele - min_ele) / num
    
        buckets = []
    
        for i in range(num):
            buckets.append([])
    

        for i in range(len(arr)):
            #diff for checking edge cases
            diff = (arr[i] - min_ele) / val_range - int((arr[i] - min_ele) / val_range)
    
            if(diff == 0 and arr[i] != min_ele):
                buckets[int((arr[i] - min_ele) / val_range) - 1].append(arr[i])
    
            else:
                buckets[int((arr[i] - min_ele) / val_range)].append(arr[i])
    
        for i in range(len(buckets)):
            if len(buckets[i]) != 0:
                insertion_sort( buckets[i] ) 
    
        k = 0
        for bucket in buckets:
            if bucket:
                for i in bucket:
                    arr[k] = i
                    k = k+1


    def merge(l1, l2):
        final = []
        
        i, j = 0, 0

        while True:
            if i == len(l1):
                while j < len(l2):
                    final.append(l2[j])
                    j += 1

            if j == len(l2):
                while i < len(l1):
                    final.append(l1[i])
                    i += 1

            if i == len(l1) and j == len(l2):
                break

            if l1[i] < l2[j]:
                final.append(l1[i])
                i += 1
            else:
                final.append(l2[j])
                j += 1

        return final

    n = len(T)

    big_buckets = []
    for i in range(len(P)):
            big_buckets.append([])

    for i in range(len(T)):
        for j in range(len(P)):
            if T[i] > P[j][0] and T[i] < P[j][1]:
                big_buckets[j].append(T[i])
                break
            
    for j in range(len(P)):
        bucket_sort(big_buckets[j], int( n * P[j][2] ))


    for i in range(1, len(P)):
        big_buckets[0] = merge(big_buckets[0], big_buckets[i])


    for i in range(len(T)):
        T[i] = big_buckets[0][i]

   


runtests( SortTab )