# Przemysław Węglik
# Pomysł polega na zserializowaniu tablicy 2D do tablicy 1D a następnie użyciu algorytmu szybkiego wybierania k-tego elemntu dwukrotnie
# aby podzielic tablicę na 3 obszary [liczby mniejsze równe przekątnej] [liczby na przekątnej] [liczby nadprzekątna]
# uzywajac linear selecta porzadkujemy tablice w taki sposob ze ponizej l znajda się tylko liczby od niego mniejsze, powyzej liczby od niego wieksza
# Podobnie z r, na prawo od niego liczby wieksze, na lewo mniejsze, a zatem miedzy l a r beda stac liczby o wartosci pomiedzy arr[l] a arr[r]
# potem pozostaje tylko deserializacja danych i wpisanie ich z powrotem do tablicy

# zlozonosci: (gdzie N - jak w tesci jeden z wymiarow tablicy 2D)
# Pamieciowa: O(N^2) na utworzenie dodatkowej tablicy 1D do zserializowania danych
# Obliczeniowa: O(N^2) - N^2 zajmuje zarowno dwukrotny quick select jak i linearyzowanie oraz delinearyzowanie tablic

from zad1testy import runtests

def Median(T):

    # select w O(n)
    def linearselect(A, k):

        def partition(arr, l, r): 
            i = (l-1)		 # index of smaller element 
            pivot = arr[r]	 # pivot 

            for j in range(l, r): 
                if arr[j] <= pivot: 
                    i = i+1
                    arr[i], arr[j] = arr[j], arr[i] 

            arr[i+1], arr[r] = arr[r], arr[i+1] 
            return i+1

        def select(A, l, r, k):
            if l == r:
                return A[l]

            q = partition(A, l, r)

            if q == k:
                return A[q]

            elif k < q:
                return select(A, l, q - 1, k)
            
            else:
                return select(A, 1 + 1, r, k)

  
        return select(A, 0, len(A) - 1, k)

    #Function to serialise data drom 2d array to 1d array
    def serialise(T, n):
        output = [0] * (n ** 2) # we know T is squre matrix

        for i in range(n):
            for j in range(n):
                output[i * n + j] = T[i][j]

        return output

    # Desrialise in a weird reverse manner from ser_arr to T
    def deserialise_inv(ser_arr, T, n, l, r):
        for i in range(n):
            for j in range(n):
                T[i][j] = 0

        for i in range(n):
            T[i][i] = ser_arr[l + i]


        k = r + 1
        for i in range(n-1):
            for j in range(i + 1, n):
                T[i][j] = ser_arr[k]
                k += 1

        
        k = 0
        for i in range(1, n):
            for j in range(i):
                T[i][j] = ser_arr[k]
                k += 1



    n = len(T)
    ser_arr = serialise(T, n)
    
    # left and right pointer for medians that will be put on diagonal
    # both l and r inclusive
    l = (n ** 2 - n)//2
    r = (n ** 2 + n)//2 - 1

    linearselect(ser_arr, l)
    linearselect(ser_arr, r)

    deserialise_inv(ser_arr, T, n, l, r)

    return T



runtests( Median ) 