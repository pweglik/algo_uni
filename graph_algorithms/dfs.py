def dfs_adj_lists(G, V):
    visited = [False] * V
    parent = [None] * V
    time = 0
    visit_time = [-1] * V
    process_time = [-1] * V

    #DFS subfunction
    def dfs_visit(G, vertex, visited, parent): # start should belong to 0 ... V - 1
        nonlocal time
        
        time += 1
        visit_time[vertex] = time

        visited[vertex] = True

        for adj_vertex in G[vertex]:
            if visited[adj_vertex] == False:
            
                parent[adj_vertex] = vertex
                dfs_visit(G, adj_vertex, visited, parent)
        
        time += 1
        process_time[vertex] = time

    #end of subfunction

    for vertex in range(V):
        if visited[vertex] == False:
            dfs_visit(G, vertex, visited, parent)

    return visited, parent, visit_time, process_time





G = [[2, 4, 5, 6], [4, 5], [0, 3, 4], [2, 7], [0, 1, 2, 5], [0, 1, 4, 8], [0, 7], [3, 6], [5,9],[8]]
V = 10
print(dfs_adj_lists(G, V))

def dfs_adj_matrix(G, V):
    visited = [False] * V
    parent = [None] * V
    time = 0
    visit_time = [-1] * V
    process_time = [-1] * V

    #DFS subfunction
    def dfs_visit(G, vertex, visited, parent): # start should belong to 0 ... V - 1
        nonlocal time
        
        time += 1
        visit_time[vertex] = time

        visited[vertex] = True

        for adj_vertex in range(V):
            if G[vertex][adj_vertex] == 1:
                if visited[adj_vertex] == False:
                
                    parent[adj_vertex] = vertex
                    dfs_visit(G, adj_vertex, visited, parent)
        
        time += 1
        process_time[vertex] = time

    #end of subfunction

    for vertex in range(V):
        if visited[vertex] == False:
            dfs_visit(G, vertex, visited, parent)

    return visited, parent, visit_time, process_time


G =[[0, 0, 1, 0, 1, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 0, 0, 0, 0],
    [1, 0, 0, 1, 1, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 1, 0, 0],
    [1, 1, 1, 0, 0, 1, 0, 0, 0, 0],
    [1, 1, 0, 0, 1, 0, 0, 0, 1, 0],
    [1, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 1, 0, 0, 1, 0, 0, 0], 
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0]]
V = 10
print(dfs_adj_matrix(G, V))