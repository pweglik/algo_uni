from copy import deepcopy

def floyd_warshall(G):
    S = deepcopy(G)
    n = len(G)

    for t in range(n):
        for u in range(n):
            for v in range(n):
                S[u][v] = min(S[u][v], S[u][t] + S[t][v])

    return S



G = [[10, 10, 3, -1, 2, 3, 6, -1, 10, 10], [10, 10, 10, 10, 1, 4, 10, 10, 10, 10], [10, 10, 10, 5, 1, 10, 4, 10, 1, 1], [1, 10, 10, 10, 10, 10, 10, 10, 10, 10], [10, 10, 10, 10, 10, 3, 10, 10, 10, 10], [10, 10, 10, 10, 10, 10, 10, 10, 10, 10], [10, 10, 10, 10, 10, 10, 10, 10, 10, 10], [10, 10, 10, 1, 10, 10, 2, 10, 10, 10], [10, 10, 10, 10, 10, 5, 10, 10, 10, 10], [10, 10, 10, 10, 10, 10, 2, 10, 1, 10]]

S = floyd_warshall(G)

for i in range(len(G)):
    print(G[i])
print("\n")
for i in range(len(S)):
    print(S[i])

