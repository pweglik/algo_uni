
def edges_to_adj_lists(G, V, directed = False):
    adj_lists = [[] for _ in range(V)]

    for edge in G:
        if directed == False:
            adj_lists[edge[0]].append(edge[1])
            adj_lists[edge[1]].append(edge[0])
        else:
            adj_lists[edge[0]].append(edge[1])

    return adj_lists

def edges_to_adj_matrix(G, V, directed = False):
    adj_matrix = [[0] * V for _ in range(V)]

    for edge in G:
        if directed == False:
            adj_matrix[edge[0]][edge[1]] = 1
            adj_matrix[edge[1]][edge[0]] = 1
        else:
             adj_matrix[edge[0]][edge[1]] = 1

    return adj_matrix

def adj_lists_to_adj_matrix(G, V):
    adj_matrix = [[0] * V for _ in range(V)]

    for i in range(V):
        for adj in G[i]:
            adj_matrix[i][adj] = 1

    return adj_matrix

def adj_matrix_to_adj_lists(G, V):
    adj_lists = [[] for _ in range(V)]

    for i in range(V):
        for j in range(V):
            if(G[i][j] == 1):
                adj_lists[i].append(j)

    return adj_lists



#undirected
G = [[0, 2], [0, 4], [0, 5], [1, 4], [1, 5], [2, 3], [2, 4], [4, 5], [0, 6], [7, 3], [7, 6], [8, 5], [9,8], [9,6], [2,6]]
V = 10

# G2 = [[0, 2], [1, 0], [2, 1], [2, 10], [10, 9], [8, 7], [7, 10], [9, 8], [7, 6], [1, 6], [6, 3], [3, 4], [4, 5], [5, 6]]
# V = 11

# print(edges_to_adj_lists(G, V))
# print(edges_to_adj_lists(G, V, True))

# print(edges_to_adj_matrix(G, V))
# print(edges_to_adj_matrix(G, V, True))

# Gl = edges_to_adj_lists(G, V)
# Gm = edges_to_adj_matrix(G, V)
# print(Gl)
# print()
# print(Gm)
# print()
# Gm = adj_lists_to_adj_matrix(Gl, V)
# Gl = adj_matrix_to_adj_lists(Gm, V)
# print(Gl)
# print()
# print(Gm)