from queue import PriorityQueue

def dijkstra(G):
    #G to lista adjacencji składująca tuple (cena, dokąd)
    q = PriorityQueue()
    n = len(G)
    parent = [-1] * n
    d = [float("inf")] * n
    visited = [False] * n
    d[0] = 0
    q.put((0,0)) # (distance, index)
    while not q.empty():
        u = q.get()
        if not visited[u[1]]:
            visited[u[1]] = True
            for i in G[u[1]]:
                if d[i[1]] > d[u[1]] + i[0]:
                    d[i[1]] = d[u[1]] + i[0]
                    parent[i] = u[1]

                q.put((d[i[1]],i[1]))
    return d

# PRZEIMPLEMENTOWAĆ NIE WKLEJAĆ