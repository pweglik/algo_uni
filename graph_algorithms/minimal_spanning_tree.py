from find_union import *
from queue import PriorityQueue

def kruskal(G, V): # verticies labeled from 0 to V-1
    G.sort(key = lambda x: x[2])

    mst = []

    sets = [None] * V

    for i in range(V):
        sets[i] = Node(i)

    for edge in G:
        if find( sets[ edge[0] ] ) != find( sets[ edge[1] ] ):
            mst.append(edge)
            union(sets[ edge[0] ], sets[ edge[1] ])

    return mst

def primm(G): # adj. lists implementation [next, cost]
    Q = PriorityQueue()
    n = len(G)
    parent = [-1] * n
    d = [float("inf")] * n
    visited = [False] * n
    d[0] = 0

    Q.put((0,0)) # (distance, index)

    while not Q.empty():
        t = Q.get()
        if visited[t[1]] == False:
            visited[t[1]] = True
            for u in G[t[1]]:
                if d[u[0]] > u[1]:
                    d[u[0]] = u[1]
                    Q.put((d[u[0]], u[0]))
                    parent[u[0]] = t[1]
    return parent


G = [[0, 2, 3], [0, 4, 2], [0, 5, 3], [1, 4, 1], [1, 5, 4], [2, 3, 5], [2, 4, 1], [4, 5, 3], 
[0, 6, 6], [7, 3, 1], [7, 6, 2], [8, 5, 5], [9, 8, 1], [9, 6, 2], [2, 6, 4]]

G_adj = [[(2, 3), (4, 2), (5, 3), (6, 6)], [(4, 1), (5, 4)], [(0, 3), (3, 5), (4, 1), (6, 4)], [(2, 5), (7, 1)], [(0, 2), (1, 1), (2, 1), (5, 3)], [(0, 3), (1, 4), (4, 3), (8, 5)], [(0, 6), (7, 2), (9, 2), (2, 4)], [(3, 1), (6, 2)], [(5, 5), (9, 1)], [(8, 1), (6, 2)]]

V = 10

print(kruskal(G, V))


print(primm(G_adj))