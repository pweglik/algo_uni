def topological_sort(G, V):
    visited = [False] * V
    parent = [None] * V
    time = 0
    visit_time = [-1] * V
    process_time = [-1] * V
    sorted_G = []

    #DFS subfunction
    def dfs_visit(G, vertex, visited, parent): # start should belong to 0 ... V - 1
        nonlocal time, sorted_G
        
        time += 1
        visit_time[vertex] = time

        visited[vertex] = True

        for adj_vertex in G[vertex]:
            if visited[adj_vertex] == False:
            
                parent[adj_vertex] = vertex
                dfs_visit(G, adj_vertex, visited, parent)
        
        time += 1
        process_time[vertex] = time
        sorted_G.append(vertex)

    #end of subfunction

    for vertex in range(V):
        if visited[vertex] == False:
            dfs_visit(G, vertex, visited, parent)

    return sorted_G

G = [[2, 4, 5, 6], [4, 5], [3, 4], [], [5], [], [], [3, 6], [5], [9]]
V = 10
print(topological_sort(G, V))