from collections import deque

def bfs_adj_lists(G, V, start): # start should belong to 0 ... V - 1
    Q = deque()
    visited = [False] * V
    distance = [-1] * V
    parent = [None] * V

    distance[start] = 0
    visited[start] = True
    Q.append(start)

    while Q:
        cur = Q.popleft()

        for adj_vertex in G[cur]:
            if visited[adj_vertex] == False:
                visited[adj_vertex] = True
                distance[adj_vertex] = distance[cur] + 1
                parent[adj_vertex] = cur
                Q.append(adj_vertex)


    return visited, distance, parent



G = [[2, 4, 5, 6], [4, 5], [0, 3, 4], [2, 7], [0, 1, 2, 5], [0, 1, 4, 8], [0, 7], [3, 6], [5,9],[8]]
V = 10
print(bfs_adj_lists(G, V, 0))


def bfs_adj_matrix(G, V, start):
    Q = deque()
    visited = [False] * V
    distance = [-1] * V
    parent = [None] * V

    distance[start] = 0
    visited[start] = True
    Q.append(start)

    while Q:
        cur = Q.popleft()

        for adj_vertex in range(V):
            if G[cur][adj_vertex] == 1:
                if visited[adj_vertex] == False:
                    visited[adj_vertex] = True
                    distance[adj_vertex] = distance[cur] + 1
                    parent[adj_vertex] = cur
                    Q.append(adj_vertex)


    return visited, distance, parent


G =[[0, 0, 1, 0, 1, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 0, 0, 0, 0],
    [1, 0, 0, 1, 1, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 1, 0, 0],
    [1, 1, 1, 0, 0, 1, 0, 0, 0, 0],
    [1, 1, 0, 0, 1, 0, 0, 0, 1, 0],
    [1, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 1, 0, 0, 1, 0, 0, 0], 
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0]]
V = 10
print(bfs_adj_matrix(G, V, 0))