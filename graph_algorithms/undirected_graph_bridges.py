def undirected_graph_bridges(G, V):
    visited = [0] * V
    parent = [None] * V
    time = 0
    visit_time = [-1] * V
    low = [-1] * V

    #DFS subfunction
    def dfs_visit(G, vertex, visited, parent): # start should belong to 0 ... V - 1
        nonlocal time
        
        time += 1
        visit_time[vertex] = time

        visited[vertex] = 1

        low[vertex] = time
        for adj_vertex in G[vertex]:
            if visited[adj_vertex] == 0:
                
                visited[adj_vertex] = 1
                parent[adj_vertex] = vertex
                dfs_visit(G, adj_vertex, visited, parent)

                low[vertex] = min(low[vertex], low[adj_vertex])
            
            elif visited[adj_vertex] == 1 and adj_vertex != parent[vertex]:
                low[vertex] = min(low[vertex], visit_time[adj_vertex])

        visited[vertex] = 2
    #end of subfunction

    for vertex in range(V):
        if visited[vertex] == False:
            dfs_visit(G, vertex, visited, parent)


    bridges = []
    for i in range(V):
        for vertex in G[i]:
            if vertex == parent[i] and low[i] == visit_time[i]:
                bridges.append([i,vertex])


    return bridges




G = [[2, 1], [0, 2, 6], [0, 1], [6, 4], [3, 5], [4, 6], [7, 1, 3, 5], [8, 10, 6], [7, 9], [10, 8], [9, 7]]
V = 11

print(undirected_graph_bridges(G, V))
