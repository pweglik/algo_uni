from collections import deque

# Without checking that it exists
# for adjecency list representation
# for matrix representation slight changes
def eulerian_circuit(G):
    circuit = deque()

    #DFS subfunction
    def dfs_visit(G, vertex): # start should belong to 0 ... V - 1
        while G[vertex]:
            next = G[vertex].pop()
            G[next].remove(vertex)

        circuit.append(vertex)
    #end of subfunction

    
    dfs_visit(G, 0)

    return circuit

G = [[2, 4, 5, 6], [4, 5], [0, 3, 4, 6], [2, 7], [0, 1, 2, 5], [0, 1, 4, 8], [0, 7, 9, 2], [3, 6], [5, 9], [8, 6]]

print(eulerian_circuit(G))
