def edges_to_adj_lists_weighted(G, V, directed = False):
    adj_lists = [[] for _ in range(V)]

    for edge in G:
        if directed == False:
            adj_lists[edge[0]].append( (edge[1], edge[2]) )
            adj_lists[edge[1]].append( (edge[0], edge[2]) )
        else:
            adj_lists[edge[0]].append( (edge[1], edge[2]) )

    return adj_lists

def edges_to_adj_matrix_weighted(G, V, directed = False):
    adj_matrix = [[0] * V for _ in range(V)]

    for edge in G:
        if directed == False:
            adj_matrix[edge[0]][edge[1]] = edge[2]
            adj_matrix[edge[1]][edge[0]] = edge[2]
        else:
             adj_matrix[edge[0]][edge[1]] = edge[2]

    return adj_matrix

def adj_lists_to_adj_matrix_weighted(G, V):
    adj_matrix = [[0] * V for _ in range(V)]

    for i in range(V):
        for tuple in G[i]:
            adj, weight = tuple
            adj_matrix[i][adj] = weight

    return adj_matrix

def adj_matrix_to_adj_lists_weighted(G, V):
    adj_lists = [[] for _ in range(V)]

    for i in range(V):
        for j in range(V):
            if(G[i][j] != 0):
                adj_lists[i].append( (j, G[i][j]) )

    return adj_lists



#undirected
G = [[0, 2, 3], [0, 4, 2], [0, 5, 3], [1, 4, 1], [1, 5, 4], [2, 3, 5], [2, 4, 1], [4, 5, 3], 
[0, 6, 6], [7, 3, 1], [7, 6, 2], [8, 5, 5], [9, 8, 1], [9, 6, 2], [2, 6, 4]]
V = 10

# G2 = [[0, 2], [1, 0], [2, 1], [2, 10], [10, 9], [8, 7], [7, 10], [9, 8], [7, 6], [1, 6], [6, 3], [3, 4], [4, 5], [5, 6]]
# V = 11

# print(edges_to_adj_lists_weighted(G, V))
# print(edges_to_adj_lists_weighted(G, V, True))

# print(edges_to_adj_matrix_weighted(G, V))
print(edges_to_adj_matrix_weighted(G, V, True))

# Gl = edges_to_adj_lists_weighted(G, V)
# Gm = edges_to_adj_matrix_weighted(G, V)
# print(Gl)
# print()
# print(Gm)
# print()
# Gm = adj_lists_to_adj_matrix_weighted(Gl, V)
# Gl = adj_matrix_to_adj_lists_weighted(Gm, V)
# print(Gl)
# print()
# print(Gm)