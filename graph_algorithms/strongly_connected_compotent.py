def strongly_connected_component(G, V):
    
    time = 0
    component = []
    process_time = [-1] * V

    #DFS subfunction
    def dfs_visit(G, vertex, visited, create_component = False): # start should belong to 0 ... V - 1
        nonlocal time, component
        visited[vertex] = True
        
        # only update time when in first DFS (e.g when return_component == False)
        if create_component == True:
            component.append(vertex)
        else:
            time += 1

        
        for adj_vertex in G[vertex]:
            if visited[adj_vertex] == False:
                dfs_visit(G, adj_vertex, visited, create_component)
        
        if create_component == False:
            time += 1
            process_time[vertex] = time

    #end of subfunction

    visited = [False] * V
    for vertex in range(V):
        if visited[vertex] == False:
            dfs_visit(G, vertex, visited)
    
    
    # Invert edges in a graph
    G_inverted = [[] for _ in range(V)]
    for i in range(V):
        for vertex in G[i]:
            G_inverted[vertex].append(i)
    
    G = G_inverted

    # Sort adjancency lists by processing time
    for i in range(V):
        G[i].sort(key = lambda x: process_time[x], reverse=True)

    
    # Sort V by p. time
    V_sorted_by_processing_time = list(range(V))
    V_sorted_by_processing_time.sort(key=lambda x: process_time[x], reverse=True)

    # Second DFS findinf SSC
    ssc = []

    visited = [False] * V
    for vertex in V_sorted_by_processing_time:
        component = []

        if visited[vertex] == False:
            dfs_visit(G, vertex, visited, True)
        if len(component) != 0:
            ssc.append(component)

    return ssc

G = [[2], [0, 6], [1, 10], [4], [5], [6], [3], [10, 6], [7], [8], [9]]
V = 11

print(strongly_connected_component(G, V))