

def bell_ford_path(G, s): # from s to everything
    n = len(G)
    parent = [None] * n
    d = [float("inf")] * n

    d[s] = 0

    for i in range(n - 1): # loop over verticies
        for j in range(n): # loop over edges
            for neighbour in G[j]: # G in form of id,weight
                if d[neighbour[0]] > d[j] + neighbour[1]:
                    d[neighbour[0]] = d[j] + neighbour[1]
                    parent[neighbour[0]] = j

    # checking negative cycles
    for j in range(n): # loop over edges
        for neighbour in G[j]: # G in form of id,weight
            if d[neighbour[0]] > d[j] + neighbour[1]:
                return False
    return d, parent



G1 = [[(1,4),(3,5)] ,[(3,5)],[(1,-10)],[(2,3)]]
s1 = 0
# False
print(bell_ford_path(G1,s1))  
G2 = [[(1, 6), (2, 5), (3, 5)], [(4, -1)], [(1, -2), (4, 1)], [(2, -2), (5, -1)], [(6, 3)], [(6, 3)], []]
s2 = 0
print(bell_ford_path(G2,s2)) 

test = [[(1,10),(2,3)],[(6,1),(5,5)],[(4,1)],[(5,8)],[(1,-4),(3,-20)],[(6,16)],[]]
test_pod_wyjebke = [[(1,10),(2,3)],[(6,1),(2,8),(5,5)],[(4,1)],[(5,8)],[(1,-88),(3,-20)],[(6,16)],[]]
# [0, 0, 3, -16, 4, -8, 1] - distance
#  [None, 4, 0, 4, 2, 3, 1]  - parent
print(bell_ford_path(test,0)) 
print(bell_ford_path(test_pod_wyjebke,0)) 