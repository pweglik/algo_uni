# Przemysław Węglik

# Wykorzystujemy algorytm Floyda-Warshalla do obliczenia długości ścieżek między każdym weirzchołkiem
# (mały preprocessing - brak krawedzi to waga inf w grafie początkowym początkowym)
# Następnie tworzymy graf dwudzielny gdzie jednym zbiorem są wierzchołki niebieskie a drugim zielone.
# Krawędz miedzy dwoma wierzchołkami tworzymy tylko wtedy kiedy najkrótsza ścieżka miedzy tymi wierzchołkami w grafie G ma długość >= D 
# (czyli żeby ta ścieżka spełniała warunek 2. z treści zadania)
# dodatkowo tworzymy źródło z krawedziami idącymi do weirzchołków zielonych i ujście z krawędziami z wierzchołków niebieskich
# (kolejność B i G nie ma tu znaczenia jako, że graf początkowy był nieskierowany, 
# ale dla wygody zastosowania max_flow graf dwudzielny tworzymy jako skierowany)
# przepustowość wszystkich krawędzi w tym grafie wynosi 1
#  wykorzystamy algorytm Edmondsa-Karpa do znalezienia maksymalnego skojarzenia w tym grafie. 
# To maksymalne skojarzenie jest takze maksymalną liczbą krawędzi o końcach niebiskim i zielonym spełniającym warunki zadania.
# (tzn. przepustowość 1 krawedzi od zrodla sprawia ze nigdy nie uzyjemy dwukrotnie tego samego wierzchołka G, 
# a przepustowość 1 krawedzi do ujścia ogranicza użycie wierzchołków B)

# złożoność obliczeniowa: O(VE^2) - złożoność algorytmu Edmondsa-Karpa, gdzie V i E to liczba wierzchołków i krawędzi w grafie początkowym
# + O(V^3) - złożoność floyda warshalla 
# złożoność całkowita: O(VE^2 + V^3)
# (liczba ta jest równa (w notacji O) z liczbą wierzchołków w grafie dwudzielnym na którym obliczamy przepływ)
# pamięciowo O(V^2) na utworzenie dodatkowych tablic S i flow_matrix (można nie tworzyć S tylko napisać T, ale wtedy ją zniszczymy)




from zad3testy import runtests
from zad3EK import edmonds_karp
from copy import deepcopy

def floyd_warshall(G):
    S = deepcopy(G)
    n = len(G)
    # modyfikacja, traktowanie krawedzi o wadze <= 0 jako brak krawedzi (waga = inf)
    for u in range(n):
        for v in range(n):
            if S[u][v] <= 0:
                S[u][v] = float("inf")

    for t in range(n):
        for u in range(n):
            for v in range(n):
                S[u][v] = min(S[u][v], S[u][t] + S[t][v])

    return S


def BlueAndGreen(T, K, D):
    n = len(T)
    S = floyd_warshall(T)

    # wierzchołki z indeksami n i n+1 to odpoweidnio źródło i ujście
    flow_matrix = [[0] * (n+2) for _ in range(n+2)] 
    

    for u in range(n):
        if K[u] == 'G':
            flow_matrix[n][u] = 1 # krawedz ze zrodła do u
        elif K[u] == 'B':
            flow_matrix[u][n+1] = 1 # krawedz z u do ujścia

        for v in range(n):
            if S[u][v] >= D and K[u] == 'G' and K[v] == 'B':
                flow_matrix[u][v] = 1 
    
    
    l = edmonds_karp(flow_matrix, n, n+1)

    return l



print(BlueAndGreen(G,K,3))
runtests( BlueAndGreen ) 
