# Przemysław Węglik

# Algorytm opiera się na obserwacji, że chcemy odciąć jak najmnijeszy node na lewo i na prawo od korzenia po prostu traversując drzewo.
# Idąc w lewo od roota idziemy tak długo dopóki nie spotkamy noda B z dwoma dziećmi.
# Wtedy podejmujemy decyzję: albo do minimalnej sumy dodajemy B.value
# albo, jeśli, oboje dzieci mają swoje dzieci to rozważamy minimum z B.value i cutthetree(B.left) + cutthetree(B.right) (rekurencyjnie)

# Analogicznie dla trawersowania drzewa po prawej stronie

# Złożoność algorytmu to O(n), gdzie n to ilość nodó w drzewie, gdyż po każdym nodzie przejdziemy maksymalnie raz. 
# Pamięciowo O(logn) na przetrzymywanie kolejnych wywołań rekurencji


from zad2testy import runtests

class BNode:
    def __init__( self, value ):
        self.left = None
        self.right = None
        self.parent = None
        self.value = value


def cutthetree(T):
    
    root = T
    min_sum = 0

    #pomocniczne przy rekurencyjnych wywołaniach
    if root.right == None and root.left == None:
        return False

    #pomocniczne przy rekurencyjnych wywołaniach, jeśli warunki spełnione nigdy nie zajdzie w całym drzewie
    if (root.right != None and root.right.right == None and root.right.left == None) and \
    (root.left != None and root.left.right == None and root.left.left == None):
        return root.value

    # od roota w prawo
    if root.right != None:
        cur = root.right
        while True:
            if cur.left != None and cur.right == None:
                cur = cur.left

            elif cur.right != None and cur.left != None:
                left = cutthetree(cur.left)
                right = cutthetree(cur.right)
                if  left != False and right != False:
                    min_sum += min(cur.value, left + right)
                else:
                    min_sum += cur.value
                break

            elif cur.left == None and cur.right != None:
                min_sum += cur.value
                break

            elif cur.left == None and cur.right == None:
                min_sum += cur.parent.value
                break
    
    # od roota w lewo
    if root.left != None:
        cur = root.left
        while True:
            if cur.left != None and cur.right == None:
                cur = cur.left
            elif cur.right != None and cur.left != None:
                left = cutthetree(cur.left)
                right = cutthetree(cur.right)
                if  left != False and right != False:
                    min_sum += min(cur.value, left + right)
                else:
                    min_sum += cur.value
                break
            elif cur.left == None and cur.right != None:  
                min_sum += cur.value
                break

            elif cur.left == None and cur.right == None:
                min_sum += cur.parent.value
                break

    return min_sum
    
            

        



    
runtests(cutthetree)


