from math import *

#Ex. A: Count of refuelings to get to last point 
# greedy algorithm - always go to the fathest available station
# Here dynamic solution:
def count_of_refuels(S, L, t):
    S.append(t)
    n = len(S)
    F = [-1] * n
    start_index = -1
    for i in range(n):
        if S[i] <= L:
            F[i] = 1
        else:
            start_index = i
            break

    for i in range(start_index, n):
        minimum = inf
        for j in range(i - 1, -1, -1):
            if S[i] - S[j] <= L:
                if F[j] < minimum:
                    minimum = F[j]
            else:
                break
        F[i] = minimum + 1
    print(S)
    print(F)
    return F[n - 1]


#Ex. B2: Minimal price of refueling to get to last point t    
# assumption we dont refuel at point t      
def count_of_refuels_with_price(S, L, C, t):
    S.append(t)
    C.append(0)
    n = len(S)
    F = [-1] * n
    start_index = -1
    for i in range(n):
        if S[i] <= L:
            F[i] = S[i] * C[i]
        else:
            start_index = i
            break

    for i in range(start_index, n):
        minimum = inf
        min_j = -1
        for j in range(i - 1, -1, -1):
            if S[i] - S[j] <= L:
                if F[j] < minimum:
                    minimum = F[j]
                    min_j = j
            else:
                break
        
        F[i] = minimum + (S[i] - S[min_j]) * C[i]


    print(S)
    print(C)
    print(F)
    return F[n - 1]




S = [3, 7, 10, 13,  14, 20, 24, 25, 31, 32]
C = [1, 2, 1, 2, 3, 1, 2, 1, 1, 0]
L = 7 

# print(count_of_refuels(S, L, t))

# print(count_of_refuels_with_price(S, L, C, t))