from queue import PriorityQueue
from copy import copy

class Node():
    def __init__(self, val, children = []):
        self.val = val
        self.children = children

    def __gt__(self, other):
        return 0

def huffman(S, F):

    def DFS(node, heap_code, codes):
        if node.children == []:
            for i in range(len(codes)):
                if codes[i][0] == node.val:
                    heap_string = ''
                    for j in range(len(heap_code)):
                        heap_string += heap_code[j]

                    codes[i][1] = heap_string
                    break
        else:
            left = node.children[0]
            right = node.children[1]

            heap_code.append('0')
            DFS(left, heap_code, codes)
            heap_code.pop()

            heap_code.append('1')
            DFS(right, heap_code, codes)
            heap_code.pop()


    queue = PriorityQueue()
    codes = []
    n = 0
    for i in range(len(S)):
        node = Node(val = S[i])
        queue.put((F[i], node))
        codes.append([S[i],node])
        n += 1

    while n > 1:
        low1 = queue.get()
        low2 = queue.get()
        new_node = Node(low1[1].val + low2[1].val, [low1[1], low2[1]])

        queue.put((low1[0] + low2[0], new_node))
        n -= 1
    
    root = queue.get()
    DFS(root[1], [], codes)
    cost = 0
    for i in range(len(codes)):
        print(codes[i][0] + ' : ' + codes[i][1])
        cost += F[i] * len(codes[i][1])

    print(cost)


  
S = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "o", "p", "r", "s", "t", "u", "w", "y", "z", "q"]
F = [865, 395, 777, 912, 431, 42, 266, 989, 524, 498, 415, 941, 803, 850, 311, 992, 489, 367, 598, 914, 930, 224, 517]
huffman(S, F)