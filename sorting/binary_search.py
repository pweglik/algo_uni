
def binary_search(arr, el, l ,r): # search position of element el in sorted array arr from l to r
    # both l and r inclusive

    while l <= r:
        mid = (l + r) // 2
        if arr[mid] < el:
            l = mid + 1
        
        elif arr[mid] > el:
            r = mid-1
        
        else:
            return mid
        
    
    return -1
