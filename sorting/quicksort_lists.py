from random import randint, seed
from time import time

class Node:
    def __init__(self, value = None):
        self.next = None
        self.value = value
    


def qsort( L ):
    # Subfunctions
    def rec_qsort(head, tail):
        if head != tail:
            ss, se, bs, be, ps, pe = partition(head, tail)
            
            
            if ss != None and se != None:
                ss, se = rec_qsort(ss, se)
                head = ss
                se.next = ps
                pe.next = None
            else:
                head = ps
                pe.next = None

            if bs != None and be != None:
                bs, be = rec_qsort(bs, be)
                pe.next = bs
                tail = be
                be.next = None
            else:
                tail = pe
                be.next = None

        return head, tail


    def partition(head, tail):
        cur = head
        pivot = head
        pivot_end = pivot

        # Add sentinel nodes, to remove amount of checking needed further
        # Delete them before merging lists
        small_start = Node(".")
        small_end = small_start
        big_start = Node(".")
        big_end = big_start
        
        # partitioning nodes into sublists smaller, bigger and equalu to pivot
        cur = cur.next
        while cur != tail.next:
            if cur.value < pivot.value:
                small_end.next = cur
                small_end = small_end.next

            elif cur.value > pivot.value:
                big_end.next = cur
                big_end = big_end.next

            else:
                pivot_end.next = cur
                pivot_end = pivot_end.next

            cur = cur.next
        
        #Deleting sentinel nodes
        small_start = small_start.next
        big_start = big_start.next

        return small_start, small_end, big_start, big_end, pivot, pivot_end

    cur = L
    while cur.next != None:
        cur = cur.next

    L, _ = rec_qsort(L, cur)

    return L





def tab2list( A ):
    H = Node()
    C = H
    for i in range(len(A)):
        X = Node()
        X.value = A[i]
        C.next = X
        C = X
    return H.next
  
  
def printlist( L ):
    while L != None:
        print( L.value, "->", end=" ")
        L = L.next
    print("|")

  
  
  

seed(42)


n = 40
T = [ randint(1,n) for i in range(n) ]
L = tab2list( T )

print("before sorting: L =", end=" ")
printlist(L)
start = time()
L = qsort(L)
end = time()

print("aftersorting    : L =", end=" ")
printlist(L)

if L == None:
    print("List is empty but it shouldnt")
    exit(0)

P = L
count = 0
while P.next != None:
    count += 1
    if P.value > P.next.value:
        print("Sorting failure")
        exit(0)
    P = P.next
 
print("OK")
print(end-start)

