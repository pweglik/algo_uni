from random import randint, seed
from time import time

def partition(arr, l, r): 
	i = (l-1)		 # index of smaller element 
	pivot = arr[r]	 # pivot 

	for j in range(l, r): 

		# If current element is smaller than or 
		# equal to pivot 
		if arr[j] <= pivot: 

			# increment index of smaller element 
			i = i+1
			arr[i], arr[j] = arr[j], arr[i] 

	arr[i+1], arr[r] = arr[r], arr[i+1] 
	return i+1


def quick_sort(arr, l, r): 
    while l < r:
        pi = partition(arr, l, r)

        if pi - l < r - pi:
        
            quick_sort(arr, l, pi - 1)
            l = pi + 1
        
        else:
            quick_sort(arr, pi + 1, r)
            r = pi - 1
    
    
# Driver code to test above 
n = 40
arr = [ randint(1,n) for i in range(n) ]
 
quick_sort(arr, 0, len(arr)-1) 
print("Sorted array is:") 
for i in range(n): 
	print(arr[i], end = " ")
