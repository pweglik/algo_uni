from random import randint, seed
from time import time

def insertion_sort(arr):
    for i in range(len(arr)):
        smallest_index = i
        for j in range(i + 1, len(arr)):
            if arr[j] < arr[smallest_index]:
                smallest_index = j
        arr[i], arr[smallest_index] = arr[smallest_index], arr[i]
    
    return arr

def bucket_sort(arr):
	buckets_arr = []
    maximum = max(arr)

	slot_num = len(arr)

	for i in range(slot_num):
		buckets_arr.append([])
		
	# Put array elements in different buckets
	for j in arr:
		index_b = int(slot_num * j)
		buckets_arr[index_b].append(j)
	
	# Sort individual buckets
	for i in range(slot_num):
		buckets_arr[i] = insertion_sort(buckets_arr[i])
		
	# concatenate the result
	k = 0
	for i in range(slot_num):
		for j in range(len(buckets_arr[i])):
			arr[k] = buckets_arr[i][j]
			k += 1
	return arr

# Driver Code
x = [0.897, 0.565, 0.656,
	0.1234, 0.665, 0.3434, 0.9, 0.3, 0.5, 0.7]
print("Sorted Array is")
print(bucket_sort(x))

