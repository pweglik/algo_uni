from random import randint, seed
from time import time


def insertion_sort(arr):
    for i in range(len(arr)):
        smallest_index = i
        for j in range(i + 1, len(arr)):
            if arr[j] < arr[smallest_index]:
                smallest_index = j
        arr[i], arr[smallest_index] = arr[smallest_index], arr[i]






n = 40
arr = [ randint(1,n) for i in range(n) ]
 
insertion_sort(arr) 
print("Sorted array is:") 
for i in range(n): 
	print(arr[i], end = " ")