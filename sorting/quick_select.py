#some kind of error
from random import randint, shuffle, seed
from math import ceil,floor,inf

def linearselect(A, k):

    def partition(arr, l, r): 
        i = (l-1)		 # index of smaller element 
        pivot = arr[r]	 # pivot 

        for j in range(l, r): 
            if arr[j] <= pivot: 
                i = i+1
                arr[i], arr[j] = arr[j], arr[i] 

        arr[i+1], arr[r] = arr[r], arr[i+1] 
        return i+1

    def select(A, l, r, k):
        if l == r:
            return A[l]

        q = partition(A, l, r)

        if q == k:
            return A[q]

        elif k < q:
            return select(A, l, q - 1, k)
        
        else:
            return select(A, 1 + 1, r, k)

  
    return select(A, 0, len(A) - 1, k)

  


A = list(range(31))
A = [1,2,4,8,16,32,64,128,256]
shuffle(A)

print(A)
a = linearselect(A, 4)
print(a)
