from random import randint, seed
from time import time


def count_sort(arr, k):  
    temp = [0] * k
    output = [0] * len(arr)


    for i in range(len(arr)):
        temp[ arr[i] ] += 1


    for i in range(1, k):
        temp [i] += temp[i - 1]


    for i in range(len(arr) - 1, -1, -1):
        temp[ arr[i] ] -= 1

        output[ temp[ arr[i] ] ] = arr[i]

    for i in range(len(arr)):
        arr[i] = output[i]




n = 20
arr = [ randint(0,n-1) for i in range(n) ]


count_sort(arr, n) 
print("Sorted array is:") 
print(arr)
print()