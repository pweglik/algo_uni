from random import randint, seed
from time import time

def left(i):
    return 2*i + 1

def right(i):
    return 2*i + 2

def parent(i):
    return (i-1)//2



def heapify(arr, n, i):
    l = left(i)
    r = right(i)
    m = i

    if l < n and arr[l] > arr[m]:
        m = l

    if r < n and arr[r] > arr[m]:
        m = r

    if m != i:
        arr[m], arr[i] = arr[i], arr[m] 
        heapify(arr, n, m)

def build_heap(arr):
    n = len(arr)

    for i in range(n//2 - 1, -1, -1):
        heapify(arr, n, i)


def heap_sort(arr):
    n = len(arr)
    build_heap(arr)
    print(arr)

    for i in range(n-1, 0, -1):
        arr[0], arr[i] = arr[i], arr[0]
        heapify(arr, i, 0)


n = 40
arr = [ randint(1,n) for i in range(n) ]


heap_sort(arr) 
print("Sorted array is:") 
print(arr)
print()
