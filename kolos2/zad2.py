# Przemysław Węglik
# Pomysł polega na uruchomieniu BFS i zapisaniu czasu odwiedzenia wierzchołków.
# Następnie uruchamiamy BFS od tyłu (od wierzchołka t) tym razem nie zapisujac czasu odwiedzenia
#  i podróżujemy tylko po krawędziach z odpowiednio mniejszymi
# czasami przetworzenia. Jeśli w którymkolwiek momencie okaże się, że ścieżka się rozdziela 
# tzn. jakiś node ma dwóch sąsiadów z distance o jeden mniejszym (idziemy w tył) lub jeśli trafimy na już odwiedzony node (to znaczy ze trafiła tam już inna ścieżka)
# to wtedy mamy dwie ścieżki idące tą samą krawędzią. usunięcie jej spowoduje wydłużenie najdłuższej ścieżki

#niestety bład w kodzie nie pozwala przejsć testów
# złożoność O(N + E) - złożoność BFSa dominuje pozostałe



from zad2testy import runtests
from collections import deque 


def enlarge(G, s, t):
    # BFS żeby znaleźć czas odwiedzenia

    V = len(G) # number of verticies
    Q = deque()
    visited = [False] * V
    distance = [-1] * V
    parent = [None] * V

    distance[s] = 0
    visited[s] = True
    Q.append(s)

    while Q:
        cur = Q.popleft()

        #sprawdzanie czy całą fala doszła już do t, jeśli tak nie ma sensu kontynuować BFSa
        if distance[t] != -1 and distance[cur] + 1 > distance[t]:
            break

        for adj_vertex in G[cur]:
            if visited[adj_vertex] == False:
                visited[adj_vertex] = True
                distance[adj_vertex] = distance[cur] + 1
                parent[adj_vertex] = cur
                Q.append(adj_vertex)

    
    edge_to_del = None


    # BFS od tyłu patrzący czy rodzic był już odwiedzony albo czy dziecko mogłoby mieć dwóch rodziców albo czy jest to jedyna ścieżka
    print(distance)
    visited = [False] * V
    visited[t] = True

    Q.append(t)

    while Q:
        cur = Q.popleft()
        children = 0
        for adj_vertex in G[cur]:
            if distance[adj_vertex] == distance[cur] - 1:
                children += 1
                if visited[adj_vertex] == False:
                    visited[adj_vertex] = True
                    Q.append(adj_vertex)

                #znaleziono parenta ktory juz jest odwiedzony znaczy ze mial inne dziecko ktore doszlo do targetu
                else:
                    if adj_vertex != s:
                        edge_to_del = (parent[adj_vertex], adj_vertex)
                        print(cur)
                        print(distance[adj_vertex], distance[cur])
                        break
                       

        if edge_to_del != None:
            break
        # jeśli jakis node ma wiecej niz 1 dziecko z dist o 1 mniejsze to znaczy
        if children > 1 and cur != t:
            for adj_vertex in G[cur]:
                if distance[adj_vertex] == distance[cur] + 1:
                    print("child")
                    edge_to_del = (cur, adj_vertex)
                    break
    
    # sprawdzenie czy jest tylko jedna sciezka wchodzaca do t i wychodzaca z s, jeśli tak i nie znaleziono wczesniej duplikatów 
    # to znaczy ze jest to jedynsa sciezka
    print(edge_to_del)
    single_path_option1 = False
    single_path_option2 = False
    flag = False
    for adj_vertex in G[s]: # start
        if distance[adj_vertex] == 1:
            if flag == True:
                single_path_option1 = True
                break

            flag = True

    flag = False
    for adj_vertex in G[s]: # target
        if distance[adj_vertex] == distance[s] - 1:
            if flag == True:
                single_path_option2 = True
                break

            flag = True

    if single_path_option1 and single_path_option2:
        edge_to_del = (parent[t], t)



    return edge_to_del

runtests( enlarge ) 
