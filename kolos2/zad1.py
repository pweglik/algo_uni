# Przemysław Weglik

# zbudujemy graf dag i wykorzstamy go do kładzenia budynków po kolei
# posortujemy budynki początkami i polaczymy krawędziami z wczesniejszego do pozniejszego jeśli nie nachodzą na siebie
# dag posortujemy topologicznie
# definiujemy funkcję rekurencyjną która dla każdego wierzchołka budynku zwróci:F(i, p) maksymalną ilość studentów przy nieprzekroczeniu limitu p
# używając tylko nodów będących na lewo (wcześniej w kolejności) w DAGu od tego i-tego wierzchołka i i-tego wierzchołka

def topological_sort(G, V):
    visited = [False] * V
    parent = [None] * V
    time = 0
    visit_time = [-1] * V
    process_time = [-1] * V
    sorted_G = []

    #DFS subfunction
    def dfs_visit(G, vertex, visited, parent): # start should belong to 0 ... V - 1
        nonlocal time, sorted_G
        
        time += 1
        visit_time[vertex] = time

        visited[vertex] = True

        for adj_vertex in G[vertex]:
            if visited[adj_vertex] == False:
            
                parent[adj_vertex] = vertex
                dfs_visit(G, adj_vertex, visited, parent)
        
        time += 1
        process_time[vertex] = time
        sorted_G.append(vertex)

    #end of subfunction

    for vertex in range(V):
        if visited[vertex] == False:
            dfs_visit(G, vertex, visited, parent)

    return sorted_G

def dfs_adj_lists(G, V):
    visited = [False] * V
    parent = [None] * V
    time = 0
   
    #DFS subfunction
    def dfs_visit(G, vertex, visited, parent): # start should belong to 0 ... V - 1
        nonlocal time
        
        time += 1

        visited[vertex] = True

        for adj_vertex in G[vertex]:
            if visited[adj_vertex] == False:
            
                parent[adj_vertex] = vertex
                dfs_visit(G, adj_vertex, visited, parent)
        
        time += 1

    #end of subfunction

    for vertex in range(V):
        if visited[vertex] == False:
            dfs_visit(G, vertex, visited, parent)



from zad1testy import runtests

def select_buildings(T, p):
    # zbudujemy graf dag i wykorzstamy go do kładzenia budynków po kolei
    # posortujemy budynki początkami i polaczymy krawędziami z wczesniejszego do pozniejszego jeśli nie nachodzą na siebie
    n = len(T)
    value = [-1] * n
    price = [-1] * n
    
    G = [[] for _ in range(n)]
    T.sort(key = lambda x: x[1])

    for i in range(len(T)):
        bud = T[i]

        value = bud[0] * (bud[2] - bud[1])
        price = bud[3]
        for j in range(i + 1, len(T)):
            bud2 = T[j]
            
            if  bud[2] <= bud2[1]:
                G[i].append(j)


    sorted_G = topological_sort(G, len(G))
    
    F = [[0] * (p+1) for _ in range(n)]

    for vertex in sorted_G:
        F[vertex][price[vertex]] = value[vertex]
 
    #nie zdążyłem dopisać reszty podejścia dynamicznego
    # trzeba użyć DFSa i znaleźć maksimum mieszkańców nie przekraczając limitu p
    # Trzeba sprawdzić jaie wartości maksymalne udało się uzyskać rodzicom w DAGu
    # rodzicom
    # możliwe, że trzeba dołożyć 3 wymiar tablicy w dynamiku aby kontrolować też cenę

    
    
    
    
    
    
    return []

runtests( select_buildings ) 
