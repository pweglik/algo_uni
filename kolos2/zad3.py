# Przemysław Węglik
# podliczenie sumarycznej wielkości plam za pomocą BFS do zmiennej oil
# potem algorytm zachłanny:
# poczynajac od pierwszego pola patrzymy na k pól do przodu gdzie k to nasz obecny zapas ropy i wybieramy pole
# z największą ilością ropy. jedziemy do niego i zbieramy całość ropy znajdującej się na tym polu
# algorytm jest poprawny z tego względu, że zawsze widząc pewne pola w swoim zasięgu
# aby pojechać dalej musimy na którymś zatankować, najlepiej zatem zatankować na polu o najwiekszym zapasie jako, że nasza cysterna nie ma limitu objetosci
# czasami wystepuj sytuacja gdy nie tankujemy na najwiekszym polu. Robimy tak gdy znajdziemy tankowanie o wartosci z tablicy refules_to_do, 
# znaczy to ze jesli nei zrobimy tego tankowania to nie wsytarczy nam paliwa na dojechanie do końca

# Złożoność O(n^3) - najbardziej negatywny przypadek? Nie jestem pewien wydaje mi isę, że to mimo wszystko O(n^2)

from zad3testy import runtests
from collections import deque

def bfs(T, start): # start should belong to 0 ... V - 1
    n = len(T)
    m = len(T[0])
    Q = deque()

    oil_sum = T[0][start]
    T[0][start] = 0

    x = 0
    y = start
    Q.append((x,y))
    
    while Q:
        
        cur = Q.popleft()
        x,y = cur
        if x > 0:
            if T[x-1][y] != 0:
                oil_sum += T[x-1][y]
                T[x-1][y] = 0
                Q.append((x-1, y))

        if x < m - 1:
            if T[x+1][y] != 0:
                oil_sum += T[x+1][y]
                T[x+1][y] = 0
                Q.append((x+1, y))

        if y > 0:
            if T[x][y-1] != 0:
                oil_sum +=  T[x][y-1]
                T[x][y-1] = 0
                Q.append((x, y-1))

        if y < n - 1:
            if T[x][y+1] != 0:
                oil_sum +=  T[x][y+1]
                T[x][y+1] = 0
                Q.append((x, y+1))


        

    return oil_sum



def plan(T): # T: n x m
    n = len(T)
    m = len(T[0])

    #liczenie paliwa
    oil = [0] * m
    for i in range(m):
        if T[0][i] != 0:
            oil[i] = bfs(T, i)

    

    # zaplamowanie tankowań - unikniecie sytuacji gdy zabraknie paliwa bo nie wykorzystamy plamy
    cp_oil = oil.copy()
    cp_oil.sort(reverse=True)
    target = m - 1
    refuels_to_do = []
    for i in range(m):
        target -= cp_oil[i]
        refuels_to_do.append(cp_oil[i])
        if target <= 0:
            break

    refuels_to_do.sort(reverse=True) #ilosc ropy w plamch z których zatankujemy



    # głowna pętla alg. zachłannego
    cur = 0
    fuel = oil[0]
    refuels = [0]
    while True:
        #jesli mozemy dojechac do konca konczymy algorytm
        if cur + fuel >= m - 1:
            break

        maximum = 0
        max_index = -1

        for i in range(1, fuel + 1):
            # sprawdzenie zaplanowanych tankowań, jeśli wystąpi jedno z nich tankujemy
            skip_max = False
            for j in range(len(refuels_to_do)):
                if oil[cur + i] == refuels_to_do[j]:
                    maximum = oil[cur + i]
                    max_index = cur + i
                    refuels_to_do.pop(j)
                    skip_max = True
                    break

            if skip_max == True:
                break

            if oil[cur + i] > maximum:
                maximum = oil[cur + i]
                max_index = cur + i

        # nie da się pojechać dalej
        if max_index == -1:
            return None
        else:
            refuels.append(max_index)
            fuel += maximum
            cur = max_index


    return refuels

runtests(plan)
